//import { v4 as uuidv4 } from "uuid";
import jwt from "jsonwebtoken";

const keyToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiY29tZnJlaXMiLCJtYWlsIjoiY29tZnJlaXNAZ21haWwuY29tIiwiaWF0IjoxNjQ2Nzc0NzAzLCJleHAiOjE2NDY3NzU5MDN9.UrRWdnq7tC0ArEWwdqYBE2vjbUKIBAOCVWZBOw9h6ao';//uuidv4(Date.now()/4);
 
module.exports = {
    async verifiedToken(req, res, next){
        const authHeader = req.headers['authorization'];
        const token = authHeader || authHeader.split(', ')[1];
        if(token == null) return res.sendStatus(401).end();

        jwt.verify(token, keyToken, (err, user) => {
            if(err) return res.json({status: 401, message: 'Bad news peoples, you shall not pass!', auth: false, token: 'Invalid token'}).end();
            
            req.user = user;
            res.json({message: 'Good news everyone, your pass!', auth: true});
            next();
        });
    },
    async verifiedAssign(req, res, next){
        jwt.sign({name: req.body.username, logged: req.body.logged}, keyToken, {expiresIn: 1200}, (err, token) => {
            if(err){
                res.json({status: 401, message: 'Bad news peoples, you shall not pass!', auth: false, token: 'Invalid token'}).end();
            }else{
                res.json({status: 200, message: 'Good news everyone, your pass!', auth: true, token: token});
                next();
            }
        }); 
    }
}



