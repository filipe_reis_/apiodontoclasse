import cors from "cors";
import morgan from "morgan";
import express from "express";
import fileUpload from "express-fileupload";
import router from "./router/_TOKEN/index";
import hosting from "./setting";
import sqlz from "./setting/connection/index";
import "./setting/db/CREATE/index";
import insertGeneral from "./setting/db/INSERT/index";
import updateGeneral from "./setting/db/UPDATE/index";
import deleteGeneral from "./setting/db/DELETE/index";
import selectGeneral from "./setting/db/SELECT/index";

const service = express();

service.use(
    express.urlencoded({extended: false}),
    express.json(),
    cors(),
    fileUpload({ createParentPath: true }),
    morgan('dev'),
    (req, res, next) => {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', false);

        // Pass to next layer of middleware
        next();
    }
);

sqlz.authenticate()
.then(
    () => {
        console.log('\nConnection has been established successfully.');
    }
).catch (
    (error) => {
        console.error('\nUnable to connect to the database:', error);
    }
);

sqlz.sync();

//GET ALL
service.get('/GET', router.verifiedToken, selectGeneral.selectAllDb);
//GET ID
service.get('/GET/id/:id', router.verifiedToken, selectGeneral.selectIdDb);
//GET USERID
service.get('/GET/userid/:userId', router.verifiedToken, selectGeneral.selectUserIdDb);
//GET USERNAME
service.get('/GET/username/:username', router.verifiedToken, selectGeneral.selectUserNameDb);
//GET USERLOGIN
service.get('/GET/userloged/:userLoged', router.verifiedToken, selectGeneral.selectUserLogedDb);

//POSTNEWUSER
service.post('/POST/newuser', router.verifiedToken, insertGeneral.insertUser);
//POSTLOGIN
service.post('/POST/login', router.verifiedAssign, insertGeneral.insertLogin);
//POSTLOGOUT
service.post('/POST/exit', router.verifiedToken, insertGeneral.insertExit);

//UPDATE
service.patch('/UPDATE/passUserId/:userId', router.verifiedToken, updateGeneral.updatePasswordUserIdDb);
//UPDATE
service.patch('/UPDATE/nameUserId/:userId', router.verifiedToken, updateGeneral.updateNameDb);

//DELETE
service.delete('/DELETE/id/:id', router.verifiedToken, deleteGeneral.deleteIdDb);
//DELETE
service.delete('/DELETE/userId/:userId', router.verifiedToken, deleteGeneral.deleteUserIdDb);
//DELETE
service.delete('/DELETE/name/:name', router.verifiedToken, deleteGeneral.deleteNameDb);

service.listen(
    hosting.PORT,
    () => {
        console.log({
            Status: 'Serviço iniciado com sucesso!',
            Host: `http://${hosting.HOST}:${hosting.PORT}`
        })
    }
)