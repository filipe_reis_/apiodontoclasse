import pool from '../../connection/index';

module.exports = {
    async updatePasswordUserIdDb(req, res) {
        let response = await pool.query(`
        UPDATE Usuarios SET password = '${req.body.password}' WHERE userId = '${req.params.userId}';
        `);

        try {
            return res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async updateNameDb(req, res) {
        let response = await pool.query(`
            UPDATE Usuarios SET name = '${req.body.name}' WHERE userId = '${req.params.userId}';
        `);

        try {
            return res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    //patch
    async updateExit(req, res) {
        let date = new Date();
        let data = date.toLocaleString('pt-BR');
        /* 
        let logData = {
            'ID': req.body.userId,
            'username': req.body.username,
            'logged': 'false',
            'sessionExit': data,
            'admin': req.body.admin
        }
         */
        let response = await pool.query(`
            UPDATE Logs SET logged = '${req.body.logged}', sessionExit = '${data}' WHERE id = ${req.body.id};
        `);

        try {
            /* let responseLogData = await pool.query(
                
                `INSERT INTO Logs(id, userId, username, logged, sessionEnter, sessionExit, admin) VALUES(
                    0,
                    '${logData.ID}',
                    '${logData.username}',
                    '${logData.logged}',
                    '',
                    '${logData.sessionExit}',
                    '${logData.admin}'
                )`,
            ); */

            return res.json(response[0]);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    }
}