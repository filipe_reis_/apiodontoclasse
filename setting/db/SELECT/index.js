import pool from '../../connection/index';

module.exports = {
    async selectAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from vw_Especials;
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectIdDb(req, res) {
        let response = await pool.query(`
            SELECT * from vw_Especial WHERE id = ${req.params.id};
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectDentistaConsultorioEspecialDb(req, res) {
        let response = await pool.query(`
            SELECT Denstista from vw_Especial WHERE 'Consultório' = ${req.params.consultorio};
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectConsultoriosAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from Consultorios;
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectConsultoriosNameDb(req, res) {
        let response = await pool.query(`
            SELECT * from Consultorios WHERE 'Consultório' = '${req.params.consultorio}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectConsultoriosIdDb(req, res) {
        let response = await pool.query(`
            SELECT * from Consultorios WHERE consultorioId = '${req.params.consultorioId}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectDentistasAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from Dentista;
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectDentistasNomeDb(req, res) {
        let response = await pool.query(`
            SELECT * from Dentista WHERE Dentista = '${req.params.dentista}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectDentistasIdDb(req, res) {
        let response = await pool.query(`
            SELECT * from Dentista WHERE dentistaId = '${req.params.dentistaId}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectPacientesAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from Pacientes;
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectPacientesNomeDb(req, res) {
        let response = await pool.query(`
            SELECT * from Pacientes WHERE Paciente = '${req.params.paciente}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectPacientesIdDb(req, res) {
        let response = await pool.query(`
            SELECT * from Pacientes WHERE pacienteId = '${req.params.pacienteId}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectServicosAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from Servicos;
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectServicosIdDb(req, res) {
        let response = await pool.query(`
            SELECT * from Servicos WHERE id = '${req.params.id}';
        `);

        try {
            res.json(response[0][0]);
        } catch (error) {
            res.json(error);
        }
    },
    async selectNotasAllDb(req, res) {
        let response = await pool.query(`
            SELECT * from Notas;
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
}