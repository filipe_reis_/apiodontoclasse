import {v4 as uuidv4} from 'uuid';
import logexport from '../../../export/log/index';
import pool from '../../connection/index';
import ramdonId from '../../ramdonId/index';

module.exports = {
    async patchVwEspecial(req, res) {
        let userData = {
            'id': req.body.id,
            'Dentista': req.body.dentista,
            'Consultorio': req.body.consultorio,
            'Paciente': req.body.paciente,
            'Servicos': req.body.servicos,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE vw_Especials SET(Dentista, Consultorio, Paciente, Servicos, Valor) VALUES(
                    '${userData.Dentista}',
                    '${userData.Consultorio}',
                    '${userData.Paciente}',
                    '${userData.Servicos}',
                    '${userData.Valor}'
                ) WHERE id = ${userData.id};`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async patchConsultorios(req, res) {
        let userData = {
            'id': req.body.id,
            'Consultorio': req.body.consultorio,
            'Dentistas': req.body.dentistas,
            'Pacientes': req.body.pacientes,
            'Servicos': req.body.servicos,
            'Telefone': req.body.telefone,
            'Endereco': req.body.endereco,
            'Porte': req.body.porte,
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE Consultorios SET(Consultorio, Dentistas, Pacientes, Servicos, Telefone, Endereco, Porte) VALUES(
                    '${userData.Consultorio}',
                    '${userData.Dentistas}',
                    '${userData.Pacientes}',
                    '${userData.Servicos}',
                    '${userData.Telefone}',
                    '${userData.Endereco}',
                    '${userData.Porte}'
                ) WHERE id = ${userData.id};`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async patchDentistas(req, res) {
        let userData = {
            'id': req.body.id,
            'Dentista': req.body.dentista,
            'Consultorio': req.body.consultorio,
            'Telefone': req.body.telefone,
            'Email': req.body.email,
            'Pacientes': req.body.pacientes,
            'Servicos': req.body.servicos
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE Dentista SET
                Dentista = '${userData.Dentista}',
                Consultorio = '${userData.Consultorio}',
                Telefone = '${userData.Telefone}',
                Email = '${userData.Email}',
                Pacientes = '${userData.Pacientes}',
                Servicos = '${userData.Servicos}'
                WHERE id = ${userData.id};`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async patchPacientes(req, res) {
        let userData = {
            'id': req.body.id,
            'Paciente': req.body.paciente,
            'Servicos': req.body.servicos,
            'Valor': req.body.valor,
            'Revisoes': req.body.revisoes
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE Pacientes SET(Paciente, Servicos, Valor, Revisoes) VALUES
                    '${userData.Paciente}',
                    '${userData.Servicos}',
                    '${userData.Valor}',
                    '${userData.Revisoes}'
                ) WHERE id = ${userData.id};`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async patchServicos(req, res) {

        let userData = {
            'ID': req.body.id,
            'Servico': req.body.servico,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE Servicos SET
                    Servico = '${userData.Servico}',
                    Valor = '${userData.Valor}'
                    WHERE id = '${userData.ID}'
                `
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async patchNotas(req, res) {

        let userData = {
            'ID': req.body.id,
            'Data': req.body.data,
            'Consultorio': req.body.consultorio,
            'Dentista': req.body.dentista,
            'Servico': req.body.servico,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `UPDATE Notas SET
                    Data = '${userData.Data}',
                    Consultorio = '${userData.Consultorio}',
                    Dentista = '${userData.Dentista}',
                    Servico = '${userData.Servico}',
                    Valor = '${userData.Valor}'
                    WHERE id = '${userData.ID}'
                `
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
}