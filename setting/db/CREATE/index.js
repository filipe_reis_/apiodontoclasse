import { DataTypes } from 'sequelize';
import pool from '../../connection/index';

const consultorio = pool.define('Consultorio',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    consultorioId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Consultorio: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Dentistas: {
        type: DataTypes.INTEGER(3),
        allowNull: false
    },
    Pacientes: {
        type: DataTypes.INTEGER(5),
        allowNull: true
    },
    Servicos: {
        type: DataTypes.INTEGER(5),
        allowNull: false
    },
    Telefone: {
        type: DataTypes.STRING(255), //boolean,
        allowNull: true
    },
    Endereco: {
        type: DataTypes.STRING(255), //boolean,
        allowNull: true
    },
    Porte: {
        type: DataTypes.STRING(255), //boolean,
        allowNull: true
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

consultorio.sync({force: false}).then(() => {
    console.log('Sucess created Consultórios table!')
}).catch((error) => {
    console.log(error)
});


const dentista = pool.define('Dentista',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    dentistaId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Dentista: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Consultorio: {
        type: DataTypes.STRING(255), //boolean,
        allowNull: false
    },
    Telefone: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    Email: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    Pacientes: {
        type: DataTypes.INTEGER(5), //boolean,
        allowNull: false
    },
    Servicos: {
        type: DataTypes.INTEGER(5), //boolean,
        allowNull: false
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

dentista.sync({force: false}).then(() => {
    console.log('Sucess created Dentistas table!')
}).catch((error) => {
    console.log(error)
});


const paciente = pool.define('Paciente',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    pacienteId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Paciente: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Servicos: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Valor: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Revisoes: {
        type: DataTypes.INTEGER(5),
        allowNull: false
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

paciente.sync({force: false}).then(() => {
    console.log('Sucess created Pacientes table!')
}).catch((error) => {
    console.log(error)
});


const viewEsp = pool.define('vw_Especial',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    vw_especialId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Dentista: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Consultorio: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Paciente: {
        type: DataTypes.STRING(255), //boolean,
        allowNull: false
    },
    Servicos: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Valor: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

viewEsp.sync({force: false}).then(() => {
    console.log('Sucess created vw_Especials table!')
}).catch((error) => {
    console.log(error)
});


const servico = pool.define('Servico',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    servicoId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Servico: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Valor: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

servico.sync({force: false}).then(() => {
    console.log('Sucess created Serviços table!')
}).catch((error) => {
    console.log(error)
});


const notas = pool.define('Notas',{
    id: {        
        type: DataTypes.INTEGER,    
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    notaId: {          
        type: DataTypes.STRING(7),
        allowNull: false
    },
    Data: {
        type: DataTypes.STRING(14),
        allowNull: false
    },
    Consultorio: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Dentista: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Servico: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    Valor: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    created_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    updated_at: {
        type: 'TIMESTAMP',
        defaultValue: pool.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false
    }
},{
    timestamps: false
});

notas.sync({force: false}).then(() => {
    console.log('Sucess created Notas table!')
}).catch((error) => {
    console.log(error)
});

module.exports = {
    consultorio,
    dentista,
    paciente,
    viewEsp,
    servico,
    notas
};