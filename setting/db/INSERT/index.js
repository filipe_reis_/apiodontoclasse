import {v4 as uuidv4} from 'uuid';
import logexport from '../../../export/log/index';
import pool from '../../connection/index';
import ramdonId from '../../ramdonId/index';

module.exports = {
    async insertVwEspecial(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'vwId': ramdonId.makeid(7),
            'Dentista': req.body.dentista,
            'Consultorio': req.body.consultorio,
            'Paciente': req.body.paciente,
            'Servicos': req.body.servicos,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO vw_Especials(id, vw_especialId, Dentista, Consultorio, Paciente, Servicos, Valor) VALUES(
                    0,
                    '${userData.vwId}',
                    '${userData.Dentista}',
                    '${userData.Consultorio}',
                    '${userData.Paciente}',
                    '${userData.Servicos}',
                    '${userData.Valor}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async insertConsultorios(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'consultorioId': ramdonId.makeid(7),
            'Consultorio': req.body.consultorio,
            'Dentistas': req.body.dentistas,
            'Pacientes': req.body.pacientes,
            'Servicos': req.body.servicos,
            'Telefone': req.body.telefone,
            'Endereco': req.body.endereco,
            'Porte': req.body.porte,
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO Consultorios(id, consultorioId, Consultorio, Dentistas, Pacientes, Servicos, Telefone, Endereco, Porte) VALUES(
                    0,
                    '${userData.consultorioId}',
                    '${userData.Consultorio}',
                    '${userData.Dentistas}',
                    '${userData.Pacientes}',
                    '${userData.Servicos}',
                    '${userData.Telefone}',
                    '${userData.Endereco}',
                    '${userData.Porte}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async insertDentistas(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'dentistaId': ramdonId.makeid(7),
            'Dentista': req.body.dentista,
            'Consultorio': req.body.consultorio,
            'Telefone': req.body.telefone,
            'Email': req.body.email,
            'Pacientes': req.body.pacientes,
            'Servicos': req.body.servicos
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO Dentista(id, dentistaId, Dentista, Consultorio, Telefone, Email, Pacientes, Servicos) VALUES(
                    0,
                    '${userData.dentistaId}',
                    '${userData.Dentista}',
                    '${userData.Consultorio}',
                    '${userData.Telefone}',
                    '${userData.Email}',
                    '${userData.Pacientes}',
                    '${userData.Servicos}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async insertPacientes(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'pacienteId': ramdonId.makeid(7),
            'Paciente': req.body.paciente,
            'Servicos': req.body.servicos,
            'Valor': req.body.valor,
            'Revisoes': req.body.revisoes
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO Pacientes(id, pacienteId, Paciente, Servicos, Valor, Revisoes) VALUES(
                    0,
                    '${userData.pacienteId}',
                    '${userData.Paciente}',
                    '${userData.Servicos}',
                    '${userData.Valor}',
                    '${userData.Revisoes}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async insertServicos(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'servicoId': ramdonId.makeid(7),
            'Servico': req.body.servico,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO Servicos(id, servicoId, Servico, Valor) VALUES(
                    0,
                    '${userData.servicoId}',
                    '${userData.Servico}',
                    '${userData.Valor}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
    async insertNotas(req, res) {
        let date = new Date();
        let id = uuidv4(date);

        let userData = {
            'ID': id,
            'notaId': ramdonId.makeid(7),
            'Data': req.body.data,
            'Consultorio': req.body.consultorio,
            'Dentista': req.body.dentista,
            'Servico': req.body.servico,
            'Valor': req.body.valor,
        };

        try {
            let responseUserData = await pool.query(
                `INSERT INTO Notas(id, notaId, Data, Consultorio, Dentista, Servico, Valor) VALUES(
                    0,
                    '${userData.notaId}',
                    '${userData.Data}',
                    '${userData.Consultorio}',
                    '${userData.Dentista}',
                    '${userData.Servico}',
                    '${userData.Valor}'
                )`
            );

            res.json(responseUserData);
        } catch (error) {
            console.log(`\n\n${error}`);
        }
    },
}