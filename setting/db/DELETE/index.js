import pool from '../../connection/index';

module.exports = {
    async deleteIdDb(req, res) {
        let response = await pool.query(`
            DELETE from vw_Especials WHERE id = ${req.params.id};
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async deleteConsultorioIdDb(req, res) {
        let response = await pool.query(`
            DELETE from Consultorios WHERE consultorioId = '${req.params.userId}';
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async deleteNameDb(req, res) {
        let response = await pool.query(`
            DELETE from Generals WHERE name = '${req.params.name}';
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async deleteNotaDb(req, res) {
        let response = await pool.query(`
            DELETE from Notas WHERE id = '${req.params.id}';
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
    async deleteServicoDb(req, res) {
        let response = await pool.query(`
            DELETE from Servicos WHERE id = '${req.params.id}';
        `);

        try {
            res.json(response[0]);
        } catch (error) {
            res.json(error);
        }
    },
}