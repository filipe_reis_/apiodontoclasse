import * as fs from 'fs';
import ramdonId from '../../setting/ramdonId';

const id = ramdonId.makeid(7);
const data = new Date();
const dataPtBr1 = data.toLocaleDateString('pt-BR').replace('/', '-');
const dataPtBr2 = dataPtBr1.replace('/', '-');

module.exports = {
    logExport(info) {
        var texto = info.toString();
        fs.writeFile(`arquivo_${dataPtBr2}_${id}.txt`, texto, (err) => {
            if (err) throw err;
            console.log('Ocorreu um erro, arquivo de log criado!');
        });
    }
}