import cors from "cors";
import morgan from "morgan";
import express from "express";
import fileUpload from "express-fileupload";
import router from "./router/_TOKEN/index";
import hosting from "./setting";
import sqlz from "./setting/connection/index";
import "./setting/db/CREATE/index";
import insert from "./setting/db/INSERT/index";
import update from "./setting/db/UPDATE/index";
import delet from "./setting/db/DELETE/index";
import select from "./setting/db/SELECT/index";
import patch from "./setting/db/PATCH/index";

const service = express();

service.use(
    express.urlencoded({extended: false}),
    express.json(),
    cors(),
    fileUpload({ createParentPath: true }),
    morgan('dev'),
    (req, res, next) => {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', false);

        // Pass to next layer of middleware
        next();
    }
);

sqlz.authenticate()
.then(
    () => {
        console.log('\nConnection has been established successfully.');
    }
).catch (
    (error) => {
        console.error('\nUnable to connect to the database:', error);
    }
);

sqlz.sync();

//GET ALL VW_ESPECIAL
service.get('/GET', select.selectAllDb);
//GET ID VW_ESPECIAL
service.get('/GET/id/:id', select.selectIdDb);
//GET CONSULTORIOALLL
service.get('/GET/consultorios', select.selectConsultoriosAllDb);
//GET DESNTISTAALLL
service.get('/GET/dentistas', select.selectDentistasAllDb);
//GET CONSULTORIOID
service.get('/GET/consultorioid/:consultorioId', select.selectConsultoriosIdDb);
//GET CONSULTORIONAME
service.get('/GET/consultorionome/:consultorionome', select.selectConsultoriosNameDb);
//GET DENTISTACONSULTORIO
service.get('/GET/dentistaconsultorio/:dentistaconsultorio', select.selectDentistaConsultorioEspecialDb);
//GET SERVICOSALLL
service.get('/GET/servicos', select.selectServicosAllDb);
//GET SERVICOSALLL
service.get('/GET/notas', select.selectNotasAllDb);

//POSTRECIBO //VW_ESPECIAL
service.post('/POST/vwEspecial', insert.insertVwEspecial);
//POSTCONSULTORIOS
service.post('/POST/consultorio', insert.insertConsultorios);
//POSTDENTISTAS
service.post('/POST/dentista', insert.insertDentistas);
//POSTPACIENTES
service.post('/POST/paciente', insert.insertPacientes);
//POSTSERVIÇOS
service.post('/POST/servico', insert.insertServicos);
//POSTSERVIÇOS
service.post('/POST/nota', insert.insertNotas);

//PATCHRECIBO //VW_ESPECIAL
service.patch('/PATCH/vwEspecial', patch.patchVwEspecial);
//PACTHCONSULTORIOS
service.patch('/PATCH/consultorio', patch.patchConsultorios);
//PATCHDENTISTAS
service.patch('/PATCH/dentista', patch.patchDentistas);
//PATCHPACIENTES
service.patch('/PATCH/paciente', patch.patchPacientes);
//PATCHSERVIÇOS
service.patch('/PATCH/servico', patch.patchServicos);
//PATCHNOTAS
service.patch('/PATCH/nota', patch.patchNotas);

//UPDATE
service.patch('/UPDATE/passUserId/:userId', update.updatePasswordUserIdDb);
//POSTLOGOUT
service.post('/UPDATE/exit', update.updateExit);
//UPDATE
service.patch('/UPDATE/nameUserId/:userId', update.updateNameDb);

//DELETE
service.delete('/DELETE/id/:id', delet.deleteIdDb);
//DELETE
service.delete('/DELETE/userId/:userId', delet.deleteConsultorioIdDb);
//DELETE
service.delete('/DELETE/name/:name', delet.deleteNameDb);
//DELETE
service.delete('/DELETE/servico/:id', delet.deleteServicoDb);
//DELETE
service.delete('/DELETE/nota/:id', delet.deleteNotaDb);

service.listen(
    hosting.PORT,
    () => {
        console.log({
            Status: 'Serviço iniciado com sucesso!',
            Host: `http://${hosting.HOST}:${hosting.PORT}`
        })
    }
)